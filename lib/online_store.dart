import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'retailstore.dart';

// ignore: camel_case_types
class onlineStore extends StatefulWidget {
  @override
  _onlineStoreState createState() => _onlineStoreState();
}

class _onlineStoreState extends State<onlineStore> {
  void showretailstore() => showCupertinoModalPopup(
        context: context,
        builder: (context) => Retailstore(),
      );

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      // physics: BouncingScrollPhysics(),
      scrollDirection: Axis.horizontal,
      child: Container(
        margin: EdgeInsets.only(top: 10),
        child: FittedBox(
          fit: BoxFit.fill,
          alignment: Alignment.topCenter,
          child: Row(
            children: <Widget>[
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showretailstore,
                child: Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'images/store.png',
                        width: 130,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showretailstore,
                child: Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'images/store.png',
                        width: 130,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showretailstore,
                child: Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'images/store.png',
                        width: 130,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showretailstore,
                child: Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'images/store.png',
                        width: 130,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showretailstore,
                child: Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'images/store.png',
                        width: 130,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showretailstore,
                child: Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'images/store.png',
                        width: 130,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showretailstore,
                child: Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'images/store.png',
                        width: 130,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showretailstore,
                child: Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'images/store.png',
                        width: 130,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showretailstore,
                child: Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'images/store.png',
                        width: 130,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class newOnlinestore extends StatefulWidget {
  @override
  _newOnlinestoreState createState() => _newOnlinestoreState();
}

class _newOnlinestoreState extends State<newOnlinestore> {
  void showretailstore() => showCupertinoModalPopup(
        context: context,
        builder: (context) => Retailstore(),
      );
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: showretailstore,
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/store.png',
                      width: 130,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
            ),
            InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: showretailstore,
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/store.png',
                      width: 130,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
            ),
            InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: showretailstore,
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/store.png',
                      width: 130,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
            ),
            InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: showretailstore,
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/store.png',
                      width: 130,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
            ),
            InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: showretailstore,
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/store.png',
                      width: 130,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
