import 'package:flutter/material.dart';

class highlight extends StatefulWidget {
  @override
  _highlightState createState() => _highlightState();
}

class _highlightState extends State<highlight> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        margin: EdgeInsets.only(top: 10),
        child: FittedBox(
          // fit: BoxFit.fill,
          alignment: Alignment.topCenter,
          child: Row(
            children: <Widget>[
              Container(
                // width: 150,
                margin: EdgeInsets.only(right: 20),
                // height: 100,
                decoration: BoxDecoration(),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/tgm.png',
                      width: 100,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
              Container(
                // width: 150,
                margin: EdgeInsets.only(right: 20),
                // height: 100,
                decoration: BoxDecoration(),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/tgm.png',
                      width: 100,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
              Container(
                // width: 150,
                margin: EdgeInsets.only(right: 20),
                // height: 100,
                decoration: BoxDecoration(
                  color: Colors.orange.shade400,
                ),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/tgm.png',
                      width: 100,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
              Container(
                // width: 150,
                margin: EdgeInsets.only(right: 20),
                // height: 100,
                decoration: BoxDecoration(
                  color: Colors.orange.shade400,
                ),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/tgm.png',
                      width: 100,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
              Container(
                // width: 150,
                // margin: EdgeInsets.only(right: 20),
                // height: 100,
                decoration: BoxDecoration(
                  color: Colors.orange.shade400,
                ),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/tgm.png',
                      width: 100,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
