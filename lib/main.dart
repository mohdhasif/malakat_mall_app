import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'promotion.dart';
import 'online_store.dart';
import 'news.dart';
import 'invest.dart';
import 'whatsnew.dart';
import 'promotionpage.dart';
import 'retailstore.dart';
import 'newspage.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final Promotions promotions = Promotions();
  ScrollController controller = ScrollController();
  bool closeTopContainer = false;
  double topContainer = 0;

  void showwhatnewmodal() => showCupertinoModalPopup(
        context: context,
        builder: (context) => whatsnew(),
      );

  void showpromotionmodal() => showCupertinoModalPopup(
        context: context,
        builder: (context) => PromotionPage(),
      );

  void showretailstore() => showCupertinoModalPopup(
        context: context,
        builder: (context) => Retailstore(),
      );

  void showBottomSheet() => showCupertinoModalPopup(
        context: context,
        builder: (context) => newspage(),
      );

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
    FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
    final Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.yellow[50],
        appBar: PreferredSize(
          child: Container(
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(
                color: Colors.yellow[700],
                offset: Offset(0, 7.0),
                blurRadius: 4.0,
              )
            ]),
            child: AppBar(
              backgroundColor: Colors.black,
              toolbarHeight: 100,
              elevation: 0.0,
              title: Image.asset('images/Group33.png'),
              centerTitle: true,
            ),
          ),
          // child: Container(
          //   decoration: BoxDecoration(boxShadow: [
          //     BoxShadow(
          //       color: Colors.yellow[700],
          //       offset: Offset(0, 7.0),
          //       blurRadius: 4.0,
          //     )
          //   ]),
          //   child: AppBar(
          //     elevation: 0.0,
          //     title: Image.asset('images/Group33.png'),
          //     centerTitle: true,
          //   ),
          // ),
          preferredSize: Size.fromHeight(100),
        ),
        // appBar: AppBar(
        //   elevation: 20,
        //   backgroundColor: Colors.black,
        //   toolbarHeight: 100,
        //   title: Image.asset('images/Group33.png'),
        //   centerTitle: true,
        //   // title: Image.asset('images/Group33.png'),
        //   // centerTitle: true,
        // ),
        body: Container(
          child: ListView(
            children: [
              // Container(
              //   height: 7,
              //   color: Colors.yellow[700],
              // ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showwhatnewmodal,
                child: Container(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          margin: EdgeInsets.only(top: 30),
                          // color: Colors.red,
                          child: Text(
                            "Highlight/ News",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: newHighligh(),
              ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showpromotionmodal,
                child: Container(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          margin: EdgeInsets.only(top: 30),
                          // color: Colors.red,
                          child: Text(
                            "Promotions",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: newPromotions(),
              ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showretailstore,
                child: Container(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          margin: EdgeInsets.only(top: 30),
                          // color: Colors.red,
                          child: Text(
                            "Online Store",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: newOnlinestore(),
              ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showBottomSheet,
                child: Container(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          margin: EdgeInsets.only(top: 30),
                          // color: Colors.red,
                          child: Text(
                            "News",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: news(),
              ),
              Container(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        margin: EdgeInsets.only(top: 30),
                        // color: Colors.red,
                        child: Text(
                          "Investment Platform",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding:
                    const EdgeInsets.only(left: 20, right: 20, bottom: 100),
                child: newInvest(),
              ),
            ],
          ),
        ),
        // body: Container(
        //   // padding: const EdgeInsets.only(left: 20, right: 20),
        //   height: size.height,
        //   child: Column(
        //     children: <Widget>[
        //       Container(
        //         height: 7,
        //         color: Colors.yellow[700],
        //       ),
        //       Expanded(
        //         child: ListView(
        //           // padding: const EdgeInsets.only(left: 20, right: 20),
        //           children: <Widget>[
        //             InkWell(
        //               splashColor: Colors.blue.withAlpha(30),
        //               onTap: showwhatnewmodal,
        //               child: Container(
        //                 padding: const EdgeInsets.only(left: 20, right: 20),
        //                 child: Column(
        //                   children: <Widget>[
        //                     Align(
        //                       alignment: Alignment.centerLeft,
        //                       child: Container(
        //                         margin: EdgeInsets.only(top: 30),
        //                         // color: Colors.red,
        //                         child: Text(
        //                           "Highlight/ News",
        //                           style: TextStyle(fontWeight: FontWeight.bold),
        //                         ),
        //                       ),
        //                     ),
        //                   ],
        //                 ),
        //               ),
        //             ),
        //             Container(
        //               padding: const EdgeInsets.only(left: 20, right: 20),
        //               child: HighlighNews(),
        //             ),
        //             InkWell(
        //               splashColor: Colors.blue.withAlpha(30),
        //               onTap: showpromotionmodal,
        //               child: Container(
        //                 padding: const EdgeInsets.only(left: 20, right: 20),
        //                 child: Column(
        //                   children: <Widget>[
        //                     Align(
        //                       alignment: Alignment.centerLeft,
        //                       child: Container(
        //                         margin: EdgeInsets.only(top: 30),
        //                         // color: Colors.red,
        //                         child: Text(
        //                           "Promotions",
        //                           style: TextStyle(fontWeight: FontWeight.bold),
        //                         ),
        //                       ),
        //                     ),
        //                   ],
        //                 ),
        //               ),
        //             ),
        //             Container(
        //               padding: const EdgeInsets.only(left: 20, right: 20),
        //               child: PromotionView(),
        //             ),
        //             InkWell(
        //               splashColor: Colors.blue.withAlpha(30),
        //               onTap: showretailstore,
        //               child: Container(
        //                 padding: const EdgeInsets.only(left: 20, right: 20),
        //                 child: Column(
        //                   children: <Widget>[
        //                     Align(
        //                       alignment: Alignment.centerLeft,
        //                       child: Container(
        //                         margin: EdgeInsets.only(top: 30),
        //                         // color: Colors.red,
        //                         child: Text(
        //                           "Online Store",
        //                           style: TextStyle(fontWeight: FontWeight.bold),
        //                         ),
        //                       ),
        //                     ),
        //                   ],
        //                 ),
        //               ),
        //             ),
        //             Container(
        //               padding: const EdgeInsets.only(left: 20, right: 20),
        //               child: onlineStore(),
        //             ),
        //             Container(
        //               padding: const EdgeInsets.only(left: 20, right: 20),
        //               child: Column(
        //                 children: <Widget>[
        //                   Align(
        //                     alignment: Alignment.centerLeft,
        //                     child: Container(
        //                       margin: EdgeInsets.only(top: 30),
        //                       // color: Colors.red,
        //                       child: Text(
        //                         "News",
        //                         style: TextStyle(fontWeight: FontWeight.bold),
        //                       ),
        //                     ),
        //                   ),
        //                 ],
        //               ),
        //             ),
        //             Container(
        //               padding: const EdgeInsets.only(left: 20, right: 20),
        //               child: news(),
        //             ),
        //             Container(
        //               padding: const EdgeInsets.only(left: 20, right: 20),
        //               child: Column(
        //                 children: <Widget>[
        //                   Align(
        //                     alignment: Alignment.centerLeft,
        //                     child: Container(
        //                       margin: EdgeInsets.only(top: 30),
        //                       // color: Colors.red,
        //                       child: Text(
        //                         "Investment Platform",
        //                         style: TextStyle(fontWeight: FontWeight.bold),
        //                       ),
        //                     ),
        //                   ),
        //                 ],
        //               ),
        //             ),
        //             Container(
        //               padding: const EdgeInsets.only(left: 20, right: 20),
        //               child: invest(),
        //             ),
        //           ],
        //         ),
        //       ),
        //     ],
        //   ),
        // ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            // Add your onPressed code here!
          },
          child: Image.asset(
            "images/Group 18.png",
            width: 130,
            fit: BoxFit.cover,
          ),
          backgroundColor: Colors.black.withOpacity(0.1),
        ),
      ),
    );
  }
}

class newHighligh extends StatefulWidget {
  @override
  _newHighlighState createState() => _newHighlighState();
}

class _newHighlighState extends State<newHighligh> {
  void showwhatnewmodal() => showCupertinoModalPopup(
        context: context,
        builder: (context) => whatsnew(),
      );
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: showwhatnewmodal,
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/tgm.png',
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
            ),
            InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: showwhatnewmodal,
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/tgm.png',
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
            ),
            InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: showwhatnewmodal,
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/tgm.png',
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
            ),
            InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: showwhatnewmodal,
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/tgm.png',
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
            ),
            InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: showwhatnewmodal,
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/tgm.png',
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
    // return Container(
    //   width: double.infinity,
    //   margin: EdgeInsets.only(top: 15),
    //   child: Row(
    //     children: [
    //       InkWell(
    //         splashColor: Colors.blue.withAlpha(30),
    //         onTap: showwhatnewmodal,
    //         child: Container(
    //           margin: EdgeInsets.only(right: 10),
    //           child: Column(
    //             children: <Widget>[
    //               Image.asset(
    //                 'images/tgm.png',
    //                 fit: BoxFit.cover,
    //               ),
    //             ],
    //           ),
    //         ),
    //       ),
    //       InkWell(
    //         splashColor: Colors.blue.withAlpha(30),
    //         onTap: showwhatnewmodal,
    //         child: Container(
    //           margin: EdgeInsets.only(right: 10),
    //           child: Column(
    //             children: <Widget>[
    //               Image.asset(
    //                 'images/tgm.png',
    //                 fit: BoxFit.cover,
    //               ),
    //             ],
    //           ),
    //         ),
    //       ),
    //       InkWell(
    //         splashColor: Colors.blue.withAlpha(30),
    //         onTap: showwhatnewmodal,
    //         child: Container(
    //           margin: EdgeInsets.only(right: 10),
    //           child: Column(
    //             children: <Widget>[
    //               Image.asset(
    //                 'images/tgm.png',
    //                 fit: BoxFit.cover,
    //               ),
    //             ],
    //           ),
    //         ),
    //       ),
    //     ],
    //   ),
    // );
  }
}

class HighlighNews extends StatefulWidget {
  @override
  _HighlighNewsState createState() => _HighlighNewsState();
}

class _HighlighNewsState extends State<HighlighNews> {
  void showwhatnewmodal() => showCupertinoModalPopup(
        context: context,
        // builder: (context) => Column(
        //   children: [
        //     ListTile(
        //       leading: Icon(Icons.share),
        //       title: Text("share"),
        //       onTap: () {},
        //     ),
        //   ],
        // ),
        builder: (context) => whatsnew(),
      );

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        margin: EdgeInsets.only(top: 10),
        child: FittedBox(
          // fit: BoxFit.fill,
          alignment: Alignment.topCenter,
          child: Row(
            children: <Widget>[
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showwhatnewmodal,
                child: Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'images/tgm.png',
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showwhatnewmodal,
                child: Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'images/tgm.png',
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showwhatnewmodal,
                child: Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'images/tgm.png',
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: () {
                  print('Card tapped.');
                  Navigator.push(
                    context,
                    CupertinoPageRoute(builder: (context) => whatsnew()),
                  );
                },
                child: Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'images/tgm.png',
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showwhatnewmodal,
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'images/tgm.png',
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Promotions extends StatelessWidget {
  const Promotions();
  // bool isList = true;
  // double aspectRatio = 0.5;

  // Widget buildContent() {
  //   if (isList) {
  //     return ListView.builder(
  //       scrollDirection: Axis.horizontal,
  //       itemBuilder: (context, index) => Padding(
  //         padding: const EdgeInsets.all(8.0),
  //         child: SizedBox(
  //           height: double.infinity,
  //           width: 300,
  //           child: Image.network(
  //             'https://source.unsplash.com/random?sig=$index',
  //             fit: BoxFit.cover,
  //           ),
  //         ),
  //       ),
  //       itemCount: 10,
  //     );
  //   } else {
  //     return GridView.builder(
  //       gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
  //         crossAxisCount: 1,
  //         mainAxisSpacing: 4,
  //         crossAxisSpacing: 4,
  //         childAspectRatio: aspectRatio,
  //       ),
  //       itemBuilder: (context, index) => SizedBox(
  //         child: Image.network(
  //           'https://source.unsplash.com/random?sig=$index',
  //           fit: BoxFit.cover,
  //         ),
  //       ),
  //       itemCount: 20,
  //       scrollDirection: Axis.horizontal,
  //     );
  //   }
  // }

  // @override
  // Widget build(BuildContext context) {
  //   return SingleChildScrollView(
  //     child: Container(
  //       child: buildContent(),
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    final double categoryHeight =
        MediaQuery.of(context).size.height * 0.30 - 50;
    return SingleChildScrollView(
      // physics: BouncingScrollPhysics(),
      scrollDirection: Axis.horizontal,
      child: Container(
        margin: EdgeInsets.only(top: 10),
        child: FittedBox(
          fit: BoxFit.fill,
          alignment: Alignment.topCenter,
          child: Row(
            children: <Widget>[
              Container(
                width: 150,
                height: categoryHeight,
                margin: EdgeInsets.only(right: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Image.asset(
                      'images/promotion.png',
                      width: 1000,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
              Container(
                width: 150,
                height: categoryHeight,
                margin: EdgeInsets.only(right: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Image.asset(
                      'images/promotion.png',
                      width: 1000,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
              Container(
                width: 150,
                height: categoryHeight,
                margin: EdgeInsets.only(right: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Image.asset(
                      'images/promotion.png',
                      width: 1000,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
              Container(
                width: 150,
                height: categoryHeight,
                margin: EdgeInsets.only(right: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Image.asset(
                      'images/promotion.png',
                      width: 1000,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // @override
  // Widget build(BuildContext context) {
  //   final double categoryHeight =
  //       MediaQuery.of(context).size.height * 0.30 - 50;
  //   return SingleChildScrollView(
  //     physics: ScrollPhysics(),
  //     child: Column(
  //       children: <Widget>[
  //         Text('Hey'),
  //         ListView.builder(
  //           physics: NeverScrollableScrollPhysics(),
  //           shrinkWrap: true,
  //           itemCount: 3,
  //           itemBuilder: (context, index) {
  //             return Text('Some text');
  //           },
  //         ),
  //       ],
  //     ),
  //   );
  // }
}
