import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'newspage.dart';

// ignore: camel_case_types
class news extends StatefulWidget {
  @override
  _newsState createState() => _newsState();
}

class _newsState extends State<news> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      // decoration: BoxDecoration(
      //   border: Border.all(color: Colors.blueAccent),
      // ),
      color: Colors.white,
      // height: 150,
      // width: 100,
      child: Center(
        child: Card(
          child: Column(
            children: [
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                // onTap: () {
                //   print('Card tapped.');
                //   Navigator.push(
                //     context,
                //     CupertinoPageRoute(builder: (context) => newspage()),
                //   );
                // },
                onTap: showBottomSheet,
                // child: Column(
                //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                //   children: [
                //     Text("this"),
                //     Text("this"),
                //     Text("this"),
                //   ],
                // ),
                child: Container(
                  height: 150,
                  width: 350,
                  decoration: BoxDecoration(
                      // color: Colors.red,
                      ),
                  child: Column(
                    children: [
                      Container(
                        // decoration: BoxDecoration(
                        //   border: Border(
                        //       top: BorderSide(
                        //     color: Colors.black,
                        //   )),
                        // ),
                        height: 90,
                        width: 100,
                        // child: Text("this"),
                      ),
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            // color: Colors.yellow,
                            border: Border(
                              top: BorderSide(
                                color: Colors.black,
                                width: 0.5,
                              ),
                            ),
                          ),
                          // height: 30,
                          width: 350,
                          child: Container(
                            padding: const EdgeInsets.all(10),
                            child: Column(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Container(
                                    child: Text(
                                      "Long Title Place Here",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Container(
                                    child: Text(
                                      "Description short place here",
                                      // style: TextStyle(
                                      //     fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void showBottomSheet() => showCupertinoModalPopup(
        context: context,
        // builder: (context) => Column(
        //   children: [
        //     ListTile(
        //       leading: Icon(Icons.share),
        //       title: Text("share"),
        //       onTap: () {},
        //     ),
        //   ],
        // ),
        builder: (context) => newspage(),
      );
}
