import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'main.dart';

class PromotionPage extends StatefulWidget {
  @override
  _PromotionPageState createState() => _PromotionPageState();
}

class _PromotionPageState extends State<PromotionPage> {
  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
    FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
    return Scaffold(
      appBar: PreferredSize(
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.yellow[700],
              offset: Offset(0, 7.0),
              blurRadius: 4.0,
            )
          ]),
          child: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: Colors.black,
            elevation: 0.0,
            title: Text("Promotions"),
            centerTitle: true,
            actions: [
              IconButton(
                icon: Icon(Icons.refresh),
                onPressed: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => super.widget));
                },
              ),
            ],
            leading: IconButton(
              icon: Icon(Icons.cancel),
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop(MyHomePage());
                // Navigator.pushAndRemoveUntil(
                //   context,
                //   MaterialPageRoute(builder: (context) => MyHomePage()),
                //   (Route<dynamic> route) => false,
                // );
              },
            ),
          ),
        ),
        // child: Container(
        //   decoration: BoxDecoration(boxShadow: [
        //     BoxShadow(
        //       color: Colors.yellow[700],
        //       offset: Offset(0, 7.0),
        //       blurRadius: 4.0,
        //     )
        //   ]),
        //   child: AppBar(
        //     elevation: 0.0,
        //     title: Image.asset('images/Group33.png'),
        //     centerTitle: true,
        //   ),
        // ),
        preferredSize: Size.fromHeight(kToolbarHeight),
      ),
      // appBar: AppBar(
      //   elevation: 4,
      //   automaticallyImplyLeading: false,
      //   backgroundColor: Colors.black,
      //   actions: [
      //     // Align(
      //     //   alignment: Alignment.centerLeft,
      //     //   child: IconButton(
      //     //     icon: Icon(Icons.cancel),
      //     //     onPressed: () {
      //     //       Navigator.pushAndRemoveUntil(
      //     //         context,
      //     //         MaterialPageRoute(builder: (context) => MyHomePage()),
      //     //         (Route<dynamic> route) => false,
      //     //       );
      //     //     },
      //     //   ),
      //     // ),
      //     IconButton(
      //       icon: Icon(Icons.refresh),
      //       onPressed: () {
      //         Navigator.pushReplacement(
      //             context,
      //             MaterialPageRoute(
      //                 builder: (BuildContext context) => super.widget));
      //       },
      //     ),
      //   ],
      //   leading: IconButton(
      //     icon: Icon(Icons.cancel),
      //     onPressed: () {
      //       Navigator.of(context, rootNavigator: true).pop(MyHomePage());
      //       // Navigator.pushAndRemoveUntil(
      //       //   context,
      //       //   MaterialPageRoute(builder: (context) => MyHomePage()),
      //       //   (Route<dynamic> route) => false,
      //       // );
      //     },
      //   ),
      //   // title: Row(
      //   //   children: [
      //   //     Icon(
      //   //       Icons.cancel,
      //   //       color: Colors.white,
      //   //       size: 30,
      //   //     )
      //   //   ],
      //   // ),
      //   title: Text("Promotions"),
      //   centerTitle: true,
      // ),
      body: Content(),
    );
  }
}

class Content extends StatefulWidget {
  @override
  _ContentState createState() => _ContentState();
}

class _ContentState extends State<Content> {
  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () {
        Navigator.pushReplacement(
          context,
          PageRouteBuilder(
            pageBuilder: (a, b, c) => PromotionPage(),
            transitionDuration: Duration(seconds: 0),
          ),
        );
        return;
      },
      child: Container(
        child: Column(
          children: [
            Expanded(
              child: ListView(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 30, left: 25, right: 25),
                    height: MediaQuery.of(context).copyWith().size.height / 3,
                    // color: Colors.indigo[200],
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Icon(Icons.arrow_downward),
                        Text("Pull to refresh",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Container(
                          child: Text("Web View from Wordpress",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
