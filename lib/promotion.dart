import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'promotionpage.dart';

class PromotionView extends StatefulWidget {
  @override
  _PromotionViewState createState() => _PromotionViewState();
}

class _PromotionViewState extends State<PromotionView> {
  void showpromotionmodal() => showCupertinoModalPopup(
        context: context,
        builder: (context) => PromotionPage(),
      );
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      // physics: BouncingScrollPhysics(),
      scrollDirection: Axis.horizontal,
      child: Container(
        margin: EdgeInsets.only(top: 10),
        child: FittedBox(
          fit: BoxFit.fill,
          alignment: Alignment.topCenter,
          child: Row(
            children: <Widget>[
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showpromotionmodal,
                child: Container(
                  width: 150,
                  margin: EdgeInsets.only(right: 20),
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'images/promotion.png',
                        width: 1000,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showpromotionmodal,
                child: Container(
                  width: 150,
                  margin: EdgeInsets.only(right: 20),
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'images/promotion.png',
                        width: 1000,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showpromotionmodal,
                child: Container(
                  width: 150,
                  margin: EdgeInsets.only(right: 20),
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'images/promotion.png',
                        width: 1000,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: showpromotionmodal,
                child: Container(
                  width: 150,
                  margin: EdgeInsets.only(right: 20),
                  child: Column(
                    children: <Widget>[
                      Image.asset(
                        'images/promotion.png',
                        width: 1000,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class newPromotions extends StatefulWidget {
  @override
  _newPromotionsState createState() => _newPromotionsState();
}

class _newPromotionsState extends State<newPromotions> {
  void showpromotionmodal() => showCupertinoModalPopup(
        context: context,
        builder: (context) => PromotionPage(),
      );
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: showpromotionmodal,
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/promotion.png',
                      width: 150,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
            ),
            InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: showpromotionmodal,
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/promotion.png',
                      width: 150,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
            ),
            InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: showpromotionmodal,
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/promotion.png',
                      width: 150,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
            ),
            InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: showpromotionmodal,
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/promotion.png',
                      width: 150,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
            ),
            InkWell(
              splashColor: Colors.blue.withAlpha(30),
              onTap: showpromotionmodal,
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'images/promotion.png',
                      width: 150,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
